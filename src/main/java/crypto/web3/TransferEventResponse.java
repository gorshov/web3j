package crypto.web3;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Uint256;

import java.math.BigInteger;

@Data
public class TransferEventResponse {

    private Address from;

    private Address to;

    private Uint256 value;

    private String transactionHash;

    private BigInteger numberOfBlock;

    @Override
    public String toString() {
        return "TransferEventResponse{" +
                "from=" + from +
                ", to=" + to +
                ", value=" + value.getValue() +
                ", transactionHash='" + transactionHash + '\'' +
                ", numberOfBlock=" + numberOfBlock +
                '}';
    }
}
