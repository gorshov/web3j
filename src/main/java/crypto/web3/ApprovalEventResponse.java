package crypto.web3;

import lombok.Data;
import lombok.ToString;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Uint256;

@Data
@ToString
public class ApprovalEventResponse {

    private Address owner;

    private Address spender;

    private Uint256 value;

    private String transactionHash;
}
