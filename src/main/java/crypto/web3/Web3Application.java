package crypto.web3;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;

import org.web3j.protocol.http.HttpService;

import org.web3j.tx.ClientTransactionManager;

import org.web3j.tx.Contract;

import java.math.BigInteger;
import java.util.List;

@SpringBootApplication
public class Web3Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Web3Application.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Web3Application.class, args);
        Web3j web3j = Web3j.build(new HttpService("https://mainnet.infura.io/v3/ebfaf3b0f741488d98214fae8e779ca1"));
        DefaultBlockParameter parameter = DefaultBlockParameter.valueOf(new BigInteger("6105935"));
        DefaultBlockParameter parameter1 = DefaultBlockParameter.valueOf(new BigInteger("6105935"));
        ClientTransactionManager transactionManager = new ClientTransactionManager(web3j, "0x43567eb78638A55bbE51E9f9FB5B2D7AD1F125aa");
        Erc20TokenWrapper token = Erc20TokenWrapper.load("0x43567eb78638A55bbE51E9f9FB5B2D7AD1F125aa", web3j, transactionManager,
                Contract.GAS_PRICE, Contract.GAS_LIMIT);
        List<TransferEventResponse> transfers = token.getLogResult(parameter1, parameter);
        for (TransferEventResponse t : transfers) {
            System.out.println(t.toString());
        }
        System.out.println("end");
    }
}
